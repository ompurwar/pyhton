from sklearn.neural_network import MLPClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn import tree

import graphviz

#[height, weight, shoe size]
X = [[181, 80, 44], [177, 70, 43], [160, 60, 38], [154, 54, 37], [166, 65, 40], [190, 90, 47], [175, 64, 39], [177, 70, 40], [171, 75, 42], [181, 85, 43]]

Y = ['male', 'female', 'female', 'female', 'male', 'male', 'male', 'female', 'male', 'female']



##################Neural Network####################
Neuralclf = MLPClassifier(solver = 'lbfgs', alpha = 1e-5, hidden_layer_sizes = (5, 2), random_state = 1)

NeuralclfModel = Neuralclf.fit(X, Y)

Neuralprediction = NeuralclfModel.predict([[190, 70, 43], [200, 70, 45]])

##################NaiveBayes######################
NaiveBayesclf = GaussianNB()
NaiveBayesclfModel = NaiveBayesclf.fit(X, Y)

NaiveBayesprediction = NaiveBayesclfModel.predict([[190, 70, 43], [200, 70, 45]])

###################Decision Tree###########################
DecisionTreeclf = tree.DecisionTreeClassifier()
DecisionTreeclfModel = DecisionTreeclf.fit(X, Y)

DecisionTreePrediction = DecisionTreeclfModel.predict([[190, 70, 43], [200, 70, 45]])

dot_data = tree.export_graphviz(DecisionTreeclf, out_file = None, 
                         feature_names = ['height', 'weight','shoe size'], 
                         class_names = ['male','female'], 
                         filled = True, rounded = True, 
                         special_characters = True)
graph = graphviz.Source(dot_data)
graph 

##################3 Printing The out puts#############
print("By NEURAL NETWORK:\t %s\nBy NAIVE_BAYES CLASSIFIER:\t%s\nBy DECISION TREE CLASSIFIER:\t%s" % (Neuralprediction, 
 NaiveBayesprediction, DecisionTreePrediction))


